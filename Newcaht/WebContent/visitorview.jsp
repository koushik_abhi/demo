
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chat</title>

	<!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    
    <link href="css/style.css" rel="stylesheet" media="screen"/>
	<link href="css/gemoticons.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="js/date-time.js"></script>
	<script src="js/jquery.gemoticons.js"></script>
	<script>
	$(document).ready(function(){
 $('#btn-input').keypress(function(event){

    	var keycode = (event.keyCode ? event.keyCode : event.which);
    	if(keycode == '13'){
           var font_style=$("select#fs option:selected").val();
    		var username="akthar";
    	var chat_msg=$(this).val();
    	$('.form-control').val('');
    	 var time=date_time();
    	$(".panel-body").append('<label style="color:#777;">'+username+'</label> : '+
         '<div class="chatbox_arrow1">'+'<span style="margin-left:5px;margin-right:5px;font-family:'+font_style+'">'+ chat_msg +  '</span>'+'</br><span id="date_time">'+time+'</span>'+'</div>'+'</br>');
        $(".panel-body").gemoticon(chat_msg);
       
    	}
    	event.stopPropagation();
    	
    });
	 $(".emotic").click(function(){
    	loademotics();
    	$('.arrow_box_emotics1').animate("slow","3000").show();
    });

	$("#chat_visitor_min").click(function(){
	  $(".chatbox_visitor_header").show();
      $(".chatbox_visitor").hide();
	  
	});
	$("#chat_min_visitor_pop").click(function(){
	   $(".chatbox_visitor").show();
	   $(".chatbox_visitor_header").hide();
	  
	});
$('.emoticlist').live('click',function(e){
   var value=$(this).attr("alt");
   console.log(value);
 var msg_prev=$('#btn-input').val();
 var msg=msg_prev+value;
 $("#btn-input").val(msg);
 $('.arrow_box_emotics1').hide();
});
	$("#fs").change(function() {
    $('#btn-input').css("font-family", $(this).val());
   });
   $("#fileupload").click(function(){
	   $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('.panel-body');
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
   
   $("#chat_close").click(function(){
	   window.location.href="login.jsp";
		  return false;
   });
   
});

function loademotics()
{
	var emoticssymobls=[":)", ":D", ";)", ":'(", ":-o", ":-/", "x-(", ":(", "B-)", ":P", "<3", ":-|"];
	for(var i=0;i<emoticssymobls.length;i++)
		{
	var el = $('<li class="emoticlist" alt="'+emoticssymobls[i]+'">'+emoticssymobls[i]+'</li>');
	
	$('.arrow_box_emotics1').append(el);
    $(el).gemoticon();
          }

}
	</script>
</head>

<body>
<div class="container">
<div class="chatbox_visitor_header">
<div class="col-md-16">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span class="glyphicon">Kenscio Chat</span> 
                        <div class="btn-group pull-right">
							<button type="button" class="btn btn-default btn-xs" id="chat_min_visitor_pop">
							  <span class="glyphicon" style="line-height:12px;font-weight:bold;">&#8212;</span>
                                        </button>
                        </div>
                    </div>
			</div>
	</div>
	</div>
	<div class="chatbox_visitor" >
        <div class="row">
            <div class="col-md-16">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span class="glyphicon">Kenscio Chat</span> 
                        <div class="btn-group pull-right">
							<button type="button" class="btn btn-default btn-xs" id="chat_visitor_min">
							  <span class="glyphicon" style="line-height:12px;font-weight:bold;text-align:center;">&#8212;</span>
                              </button>
							<button type="button" class="btn btn-default btn-xs" id="chat_close">
                                <span class="glyphicon">X</span>
                            </button>
                        </div>
                    </div>
                <div class="visitorpanel-body">
               <div style="width: 30%; float:left;">
                    <div id="lefcontent">
                    <div class="panel panel-primary">
                       <div class="panel-body">
                       
                       </div>
                       <div class="panel-footer">
					    <div style="height:25px;">
						<div class="octicon-tag" title="Attach Files">
						<button type="button" class="btn btn-primary btn-xs fileinput-button" style="margin-left:5px;">
                                <span class="glyphicon"><b>Attachments</b>
								<input id="fileupload" type="file" name="files" />
								</span>
                            </button>
						</div>
						<select id="fs" style="width:100px;margin-top:5px;margin-left:5px;height:20px;"> 
                         <option value="Arial">Arial</option>
                         <option value="Verdana ">Verdana </option>
                         <option value="Impact ">Impact </option>
                         <option value="Comic Sans MS">Comic Sans MS</option>
                         </select>
						<div class="emotic" title="smileys"></div>
						</div>
                        <div class="input-group">
                            <input id="btn-input" type="text" class="form-control chat_data" placeholder="Type your message here..." size="50" value=""/>
                        </div>
                    </div>
                    </div>
                    </div>
               </div>

              <div style="width: 70%; float:right;">
               <div class="panel panel-primary">
              <ul id="myTab" class="nav nav-tabs">
   <li >
      <a href="#locality" data-toggle="tab">
        Locality
      </a>
   </li>
   <li><a href="#demographic" data-toggle="tab">Demographic</a></li>
      <li><a href="#transactional" data-toggle="tab">Transactional</a></li>
   
      <li><a href="#behavioural" data-toggle="tab">Behavioural</a></li>
   
      <li class="active"><a href="#interest" data-toggle="tab">Interests</a></li>
   
</ul>
<div id="myTabContent" class="tab-content" style="height:330px;">
   <div class="tab-pane fade " id="locality">
      <p>Locality</p>
   </div>
   <div class="tab-pane fade" id="demographic">
      <p>Demographic</p>
   </div>
   <div class="tab-pane fade" id="transactional">
      <p>Transactional</p>
   </div>
   <div class="tab-pane fade" id="behavioural">
      <p>Behavioural.</p>
   </div>
   <div class="tab-pane fade in active" id="interest">
      <p>Interests</p>
       <p>Enterprise Java Beans (EJB) is a development architecture 
         for building highly scalable and robust enterprise level    
         applications to be deployed on J2EE compliant 
         Application Server such as JBOSS, Web Logic etc.
      </p>
       <p>Enterprise Java Beans (EJB) is a development architecture 
         for building highly scalable and robust enterprise level    
         applications to be deployed on J2EE compliant 
         Application Server such as JBOSS, Web Logic etc.
      </p>
       <p>Enterprise Java Beans (EJB) is a development architecture 
         for building highly scalable and robust enterprise level    
         applications to be deployed on J2EE compliant 
         Application Server such as JBOSS, Web Logic etc.
      </p>
       <p>Enterprise Java Beans (EJB) is a development architecture 
         for building highly scalable and robust enterprise level    
         applications to be deployed on J2EE compliant 
         Application Server such as JBOSS, Web Logic etc.
         Application Server
      </p>
       
   </div>
</div>
               </div>
                        
                    
                </div> 
                </div>
                </div>
            </div>
        </div>
		<div class="arrow_box_emotics1" style="display:none;"></div>
		</div>
		
    </div>

</body>
</html>
