
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chat</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen" />

<link href="css/style.css" rel="stylesheet" media="screen" />
<link href="css/gemoticons.css" type="text/css" rel="stylesheet" />

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/date-time.js"></script>
<script src="js/jquery.gemoticons.js"></script>
<script type="text/javascript" src="js/facebook.js"></script>
<script>
	$(document).ready(function(){
		$("#facelogin").click(function(){
			 FB.login(function(response) {
				   if (response.authResponse) {
				     console.log('Welcome!  Fetching your information.... ');
				     FB.api('/me', function(response) {
				    	 
				   	      var name=response.name;
				   	       alert(name);
				   	   
				   	   window.location.href="chatmain.jsp?name="+name+"";
				       console.log('Good to see you, ' + response.name + '.');
				    
				     });
				     
				   } else {
				     console.log('User cancelled login or did not fully authorize.');
				   }
				 });
		});
		  window.fbAsyncInit = function() {
			  FB.init({
			    appId      : '279753312198447',
			    cookie     : true,  
			                        
			    xfbml      : true,  
			    status     : true,
			    oauth      : true,
			    version    : 'v2.0'
			  });
		  };
	$("#chat_min").click(function(){
	  $(".chatbox_header_login").show();
      $(".chatbox_login").hide();
	  
	});
	$("#chat_min_pop").click(function(){
	   $(".chatbox_login").show();
	   $(".chatbox_header_login").hide();
	  
	});

   $("button[type='submit']").click(function(){
     window.location.href="chatmain.jsp";
	  return false;
   });
	});
	</script>
</head>

<body>
	<div class="chatbox_header_login">
		<div class="col-md-16">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<span class="glyphicon">Chat Now</span>
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-default btn-xs"
							id="chat_min_pop">
							<span class="glyphicon"
								style="line-height: 12px; font-weight: bold;">&#8212;</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="chatbox_login">
		<div class="row">
			<div class="col-md-16">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<span class="glyphicon">Kenscio Chat</span>
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-default btn-xs"
								id="chat_min">
								<span class="glyphicon"
									style="line-height: 12px; font-weight: bold; text-align: center;">&#8212;</span>
							</button>
						</div>
					</div>
					<div class="panel-body">
						<form role="form">
							<div class="form-group">
								<label for="username">User Name <span style="color: red">*</span></label>
								<input type="text" class="form-control" id="username"
									placeholder="Enter User Name" />
							</div>
							<div class="form-group">
								<label for="email">Email <span style="color: red">*</span></label>
								<input type="text" class="form-control" id="email"
									placeholder="Email" />
							</div>
							<button type="submit" class="btn buttonOrange pull-right">Start
								Chat</button>
						</form>
						<div class="form-group" style="margin-top: 50px;">
							<div align="center">
								<label style="font-size: 12px; margin-top: 10px;">OR</label>
								<hr />
							</div>
							<div align="center">
								<label
									style="font-size: 12px; margin-right: 10px; margin-top: 10px;">Loginwith:</label>
								    <img src="images/linkedin.png" alt="linked in" title="Linked In"
									class="social" />&nbsp;<img src="images/facebook.png" id="facelogin"
									alt="facebook" title="Facebook" class="social" diasabled />&nbsp;<img
									src="images/twitter.png" alt="twitter" title="Twitter"
									class="social" diasabled />
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<div align="center">
							<label for="loginsocial" style="font-size: 12px;margin-top:5px;">Copyright
								2012-2014 Kenscio. All Rights Reserved</label>
						</div>
						<div align="center">
							<a href="#">www.kenscio.com</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

</body>
</html>
